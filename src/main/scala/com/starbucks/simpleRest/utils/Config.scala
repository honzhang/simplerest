package com.starbucks.simpleRest.utils

import com.typesafe.config.ConfigFactory

/**
  * Project: simplerest
  * Author: honzhang on 10/11/2016.
  */
trait Config {
  private val config = ConfigFactory.load()
  private val httpConfig = config.getConfig("http")
  private val databaseConfig = config.getConfig("database")

  val httpHost = httpConfig.getString("host")
  val httpPort = httpConfig.getInt("port")
}
