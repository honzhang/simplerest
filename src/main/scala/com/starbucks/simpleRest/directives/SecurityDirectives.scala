package com.starbucks.simpleRest.directives

import akka.http.scaladsl.server.Directive1

/**
  * Project: simplerest
  * Author: honzhang on 10/12/2016.
  */
trait SecurityDirectives {
  def authenticate = {
    true
  }
}
