package com.starbucks.simpleRest.http.routes

import akka.http.scaladsl.server.Directives._
import com.starbucks.simpleRest.directives.SecurityDirectives
import com.starbucks.simpleRest.services.CustomerSubscriberService
import de.heikoseeberger.akkahttpcirce.CirceSupport
import io.circe.generic.auto._
import io.circe.syntax._

/**
  * Project: simplerest
  * Author: honzhang on 10/12/2016.
  */
class CustomerSubscriberServiceRoute(val service: CustomerSubscriberService)
  extends CirceSupport with SecurityDirectives {

  import service._

  val route = pathPrefix("customer") {
    pathEndOrSingleSlash {
      get {
        complete(getter())
      }
    }
  }
}
