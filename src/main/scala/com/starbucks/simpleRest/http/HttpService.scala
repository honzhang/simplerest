package com.starbucks.simpleRest.http

import com.starbucks.simpleRest.http.routes.CustomerSubscriberServiceRoute
import com.starbucks.simpleRest.services.CustomerSubscriberService
import akka.http.scaladsl.server.Directives._

import scala.concurrent.ExecutionContext

/**
  * Project: simplerest
  * Author: honzhang on 10/12/2016.
  */
class HttpService(customerSubscriberService: CustomerSubscriberService)
                 (implicit exexcutionContext: ExecutionContext) extends CorsSupport {

  val customerRoute = new CustomerSubscriberServiceRoute(customerSubscriberService)

  val routes =
    pathPrefix("v1") {
      corsHandler(
        customerRoute.route
      )
    }
}
