package com.starbucks.simpleRest.services

/**
  * Project: simplerest
  * Author: honzhang on 10/12/2016.
  */
class BaseService {

}

abstract class BaseSubscriberService extends BaseService {
  def getter(): String = "Invalid Request for a Subscriber Service"
}
