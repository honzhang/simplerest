package com.starbucks.simpleRest

import akka.actor.ActorSystem
import akka.event.{Logging, LoggingAdapter}
import akka.http.scaladsl.Http
import akka.stream.ActorMaterializer
import com.starbucks.simpleRest.http.HttpService
import com.starbucks.simpleRest.services.CustomerSubscriberService
import com.starbucks.simpleRest.utils.Config

import scala.concurrent.ExecutionContext

object Main extends App with Config{
  println("server is starting....")

  implicit val actorSystem = ActorSystem()
  implicit val executor: ExecutionContext = actorSystem.dispatcher
  implicit val log: LoggingAdapter = Logging(actorSystem, getClass)
  implicit val materializer: ActorMaterializer = ActorMaterializer()

  val customerService = new CustomerSubscriberService()
  val httpService = new HttpService(customerService)

  Http().bindAndHandle(httpService.routes, httpHost, httpPort)
}
