# README #

this is a simple rest service project for POC purpose only. some key characteristics below: 

* uses AkkaHttp 
* supports versioning (of the service)
* supports CORS 
* endpoint configurable 
* route and service implementation separated 

# RUN # 

just type "sbt run", the server will launch. 
go to: http://127.0.0.1:9001/v1/customer 

available endpoints are coming....

# TODO #

* data model in/out 
* arguments 
* logging 
* emit message to another service with Kafka 
* database support with Cassandra
* persistent actor with DB storage 

# Considerations #

* authentication
* rule engine
* deployment/publish